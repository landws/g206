package com.simmax.g206;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {
	// 设置此handler处理所有异常
	@ExceptionHandler(value = Exception.class)
	public void defaultErrorHandler() {
		System.out.println("-------------default error");
	}
}
