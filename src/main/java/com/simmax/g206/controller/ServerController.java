package com.simmax.g206.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simmax.g206.service.BackMonitorService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.processors.JsonValueProcessor;

@Controller
@RequestMapping(path = "/server")
public class ServerController {
	private static final Logger logger = LoggerFactory.getLogger(ServerController.class);
	JsonConfig config = new JsonConfig();
	public static final String SESSION_LOGINUSER = "G206_LoginUser";

	public ServerController() {
		config.registerJsonValueProcessor(Date.class, new JsonValueProcessor() {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

			public Object processObjectValue(String propertyName, Object date, JsonConfig config) {
				return simpleDateFormat.format(date);
			}

			public Object processArrayValue(Object date, JsonConfig config) {
				return simpleDateFormat.format(date);
			}
		});
	}

	@Autowired
	BackMonitorService backmonitor;

	@ResponseBody
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public Object login(@RequestParam("user") String user, @RequestParam("pwd") String pwd, HttpSession httpSession) {
		JSONObject json = new JSONObject();
		boolean success = backmonitor.login(user, pwd);
		json.put("success", success);
		if (success) {
			httpSession.setAttribute(SESSION_LOGINUSER, user);
			httpSession.setMaxInactiveInterval(0);
		}
		return json;
	}

	@RequestMapping(value = "/logout")
	public Object logout(HttpSession httpSession) {
		JSONObject json = new JSONObject();
		json.put("success", "ture");
		httpSession.setAttribute(SESSION_LOGINUSER, "");
		return json;
	}

	@ResponseBody
	@RequestMapping(value = "/devices")
	public Object devices(HttpSession httpSession) {
		Object user = httpSession.getAttribute(SESSION_LOGINUSER);
		Object[] devices = backmonitor.getDevices(user.toString());
		JSONArray json = JSONArray.fromObject(devices, config);
		return json;
	}

	@ResponseBody
	@RequestMapping(value = "/device/{id}")
	public Object deviceone(@PathVariable("id") String id, HttpSession httpSession) {
		Object user = httpSession.getAttribute(SESSION_LOGINUSER);
		Object device = backmonitor.getDevice(user.toString(), id);
		JSONObject json = JSONObject.fromObject(device, config);
		return json;
	}

	@ResponseBody
	@RequestMapping(value = "/statistics")
	public Object statistics(HttpSession httpSession) {
		Object user = httpSession.getAttribute(SESSION_LOGINUSER);
		Object statistics = backmonitor.getStatistics(user.toString());
		if (statistics != null) {
			JSONObject json = JSONObject.fromObject(statistics, config);
			return json;
		}
		return new JSONObject();
	}

	@ResponseBody
	@RequestMapping(value = "/alarm")
	public Object alarm(HttpSession httpSession) {
		Object user = httpSession.getAttribute(SESSION_LOGINUSER);
		Map alarms = backmonitor.getAlarms(user.toString());
		JSONArray json = JSONArray.fromObject(alarms, config);
		return json;
	}

	@ResponseBody
	@RequestMapping(value = "/alarm/{id}")
	public Object alarmone(@PathVariable("id") String id, HttpSession httpSession) {
		Object user = httpSession.getAttribute(SESSION_LOGINUSER);
		Map alarms = backmonitor.getAlarms(user.toString());
		if (alarms.containsKey(id)) {
			return JSONObject.fromObject(alarms.get(id), config);
		}
		return new JSONObject();
	}

	@ResponseBody
	@RequestMapping(value = "/histories/{id}")
	public Object history(@PathVariable("id") String id, HttpSession httpSession) {
		Object user = httpSession.getAttribute(SESSION_LOGINUSER);
		Double[] values = backmonitor.getHistories(user.toString(), id);
		JSONArray json = JSONArray.fromObject(values, config);
		return json;
	}

	@ResponseBody
	@RequestMapping(value = "/realtime/{id}")
	public Object realtime(@PathVariable("id") String id, HttpSession httpSession) {
		Object user = httpSession.getAttribute(SESSION_LOGINUSER);
		Object realtime = backmonitor.getRealtime(user.toString(), id);
		if (realtime != null) {
			JSONObject json = JSONObject.fromObject(realtime, config);
			return json;
		}
		return new JSONObject();
	}
}
