package com.simmax.g206.device;

import java.util.Date;

public class DeviceData {
	private String id;//设备ID
	private double radiationdose;//剂量率
	private int quantityOfElectric;//电量
	private int status;//状态
	private Date time = new Date();

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public double getRadiationdose() {
		return radiationdose;
	}

	public void setRadiationdose(double radiationdose) {
		this.radiationdose = radiationdose;
	}

	public int getQuantityOfElectric() {
		return quantityOfElectric;
	}

	public void setQuantityOfElectric(int quantityOfElectric) {
		this.quantityOfElectric = quantityOfElectric;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
}
