package com.simmax.g206.device;

public class StatisticsInfo {
	private int offlines;
	private int normals;
	private int middleWarns;
	private int highWarns;
	private int warns;

	public StatisticsInfo() {

	}

	public int getOfflines() {
		return offlines;
	}

	public void setOfflines(int offlines) {
		this.offlines = offlines;
	}

	public int getNormals() {
		return normals;
	}

	public void setNormals(int normals) {
		this.normals = normals;
	}

	public int getMiddleWarns() {
		return middleWarns;
	}

	public void setMiddleWarns(int middleWarns) {
		this.middleWarns = middleWarns;
	}

	public int getHighWarns() {
		return highWarns;
	}

	public void setHighWarns(int highWarns) {
		this.highWarns = highWarns;
	}

	public int getWarns() {
		return warns;
	}

	public void setWarns(int warns) {
		this.warns = warns;
	}

}
