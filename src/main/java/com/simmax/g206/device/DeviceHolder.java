package com.simmax.g206.device;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

import org.springframework.stereotype.Service;

@Service
public class DeviceHolder {
	private Map<String, DeviceInfo> devices = new HashMap<String, DeviceInfo>();

	private Map<String, DeviceData> alarms = new HashMap<String, DeviceData>();

	private Map<String, Queue<Double>> dataOfSeconds = new HashMap<String, Queue<Double>>();

	// private Queue<StatisticsInfo> statistics = new
	// ArrayBlockingQueue<StatisticsInfo>(30, true);

	public DeviceHolder() {
		// TODO Auto-generated constructor stub
	}

	public Map getAlrams() {
		return alarms;
	}

	public void addStatistics(StatisticsInfo st) {
		// statistics.add(st);
	}

	public StatisticsInfo getStatistics() {
		StatisticsInfo info = new StatisticsInfo();
		Iterator<DeviceInfo> iterator = devices.values().iterator();
		while (iterator.hasNext()) {
			DeviceInfo next = iterator.next();
			switch (next.getStatus()) {
			case 0:
				info.setOfflines(info.getOfflines() + 1);
				break;
			case 1:
				info.setNormals(info.getNormals() + 1);
				break;
			case 2:
				info.setMiddleWarns(info.getMiddleWarns() + 1);
				break;
			case 3:
				info.setHighWarns(info.getHighWarns() + 1);
				break;
			case 4:
				info.setWarns(info.getWarns() + 1);
				break;
			}
		}
		// if (statistics.size() > 0)
		// return statistics.peek();
		return info;
	}

	public synchronized void addAlarm(String id, double rd, int qoe, int status) {
		DeviceData data = null;
		if (!alarms.containsKey(id)) {
			alarms.put(id, new DeviceData());
		}
		data = alarms.get(id);
		data.setId(id);
		data.setRadiationdose(rd);
		data.setQuantityOfElectric(qoe);
		data.setStatus(status);
		data.setTime(new Date());
	}

	public int getDeviceCount() {
		return devices.size();
	}

	public DeviceInfo[] getDevices() {
		return devices.values().toArray(new DeviceInfo[] {});
	}

	public void clear() {
		devices.clear();
	}

	public DeviceInfo getDevice(String id) {
		if (devices.containsKey(id))
			return devices.get(id);
		return null;
	}

	public synchronized void addDevice(DeviceInfo device) {
		if (!devices.containsKey(device.getId())) {
			devices.put(device.getId(), device);
		} else {
			DeviceInfo info = devices.get(device.getId());
			info.setName(device.getName());
			// info.setStatus(device.getStatus());
		}
	}

	public synchronized void addDevice(String id, String name, int status) {
		DeviceInfo info = new DeviceInfo();
		info.setId(id);
		info.setName(name);
		info.setStatus(status);
		addDevice(info);
	}

	private void ensureDataOfSeconds(String id) {
		if (!dataOfSeconds.containsKey(id)) {
			dataOfSeconds.put(id, new ArrayBlockingQueue<Double>(300, true));
		}
	}

	public synchronized void addDataOfSeconds(String id, double value) {
		ensureDataOfSeconds(id);
		Queue<Double> queue = dataOfSeconds.get(id);
		if (queue.size() >= 300) {
			queue.poll();
		}
		queue.add(value);
	}

	public synchronized int getDataOfSecondsCount(String id) {
		ensureDataOfSeconds(id);
		return dataOfSeconds.get(id).size();
	}

	public synchronized Double[] getDataOfSeconds(String id) {
		ensureDataOfSeconds(id);
		return dataOfSeconds.get(id).toArray(new Double[0]);
	}
}
