package com.simmax.g206.device;

import java.util.Date;

public class DeviceInfo {
	// a) 设备ID：设备ID号
	private String id;
	// b) 名称：设备名称，含中文，编码方式GB2312
	private String name;
	// c) 设备状态：0离线，1正常，2中预警，3高预警，4报警
	private int status;

	private DeviceData lastData = new DeviceData();

	private Date time = new Date();

	public DeviceData getLastData() {
		return lastData;
	}

	public void setLastData(double rd, int qoe, int status) {
		setLastData(this.getId(), rd, qoe, status);
	}

	private void setLastData(String id, double rd, int qoe, int status) {
		lastData.setId(id);
		lastData.setRadiationdose(rd);
		lastData.setQuantityOfElectric(qoe);
		lastData.setStatus(status);
		lastData.setTime(new Date());
		
		this.status = status;
	}

	public String getId() {
		return id;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public void setId(String id) {
		this.id = id;
		this.lastData.setId(id);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
}
