package com.simmax.g206;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class AnnotationSecurityConfiguration implements WebMvcConfigurer {

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		System.out.println("addInterceptors SecurityFilter");
		registry.addInterceptor(new SecurityFilter());
		//.addPathPatterns("/server");
	}

}