package com.simmax.g206.service;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.Queue;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.simmax.g206.device.DeviceHolder;
import com.simmax.g206.device.DeviceInfo;
import com.simmax.g206.device.StatisticsInfo;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;

public class BackMonitorDaemon implements DisposableBean, Runnable, ClientListener {
	private static final Logger logger = LoggerFactory.getLogger(BackMonitorDaemon.class);

	private Thread thread;
	private volatile boolean running;

	@Autowired
	private DeviceHolder deviceHolder;
	// 106.15.204.113:10300
	@Autowired
	NettyClient nettyClient;

	public BackMonitorDaemon() {

	}

	@PostConstruct
	public void init() {
		nettyClient.addListener(this);
		logger.info("add listener to " + nettyClient);

		this.thread = new Thread(this);
	}

	public void start() {
		this.thread.start();
	}

	public DeviceHolder getDeviceHolder() {
		// System.out.println(deviceHolder);
		return deviceHolder;
	}

	@Override
	public void channelRead(String msg) {
		System.out.println("====================");
		logger.info(msg);
		try {
			if (msg.startsWith("{A0") && msg.endsWith("]}")) {
				// [1,设备1,3][2,设备2,3][3,设备3,4][4,设备4,4][5,设备5,1][6,设备6,4][7,设备7,2][8,设备8,4][9,设备9,3][10,设备10,2][11,设备11,2][12,设备12,3][13,设备13,3][14,设备14,3][15,设备15,2][16,设备16,4][17,设备17,3][18,设备18,1][19,设备19,0][20,设备20,1][21,设备21,3][22,设备22,1][23,设备23,2][24,设备24,2][25,设备25,4][26,设备26,3][27,设备27,3][28,设备28,2][29,设备29,1][30,设备30,1]
				String devicestr = msg.substring(3, msg.length() - 1);
				System.out.println(devicestr);
				String[] strings = devicestr.split("]");
				for (String devstr : strings) {
					// System.out.println(devstr);
					String[] devs = devstr.substring(1).split(",");
					int status = Integer.parseInt(devs[2]);

					getDeviceHolder().addDevice(devs[0], devs[1], status);
				}
			} else if (msg.startsWith("{A2") && msg.endsWith("]}")) {
				// 设备ID1,剂量率,电量,设备状态
				// {A2[2,4.7435072E7,50,4][3,6.542704E7,20,4][5,6138398.5,10,4][6,5.9807024E7,20,4][7,1.0326731E7,70,4][9,6.9244536E7,70,4][11,7.0619664E7,70,4][15,870663.9,40,4][17,6.2861088E7,80,4]}
				String alarmstr = msg.substring(3, msg.length() - 1);
				System.out.println(alarmstr);
				String[] strings = alarmstr.split("]");
				for (String alastr : strings) {
					// System.out.println(alastr);
					String[] values = alastr.substring(1).split(",");
					String id = values[0];
					double rd = Double.parseDouble(values[1]);
					int qoe = Integer.parseInt(values[2]);
					int status = Integer.parseInt(values[3]);

					getDeviceHolder().addAlarm(id, rd, qoe, status);
				}
			} else if ((msg.startsWith("{A1") || msg.startsWith("{A4")) && msg.endsWith("]}")) {
				// {A1[统计信息][设备状态列表]}
				// {A1[7,4,7,5,7][1,1.1917635,50,2][2,5.110202E7,60,4][27,0.0,10,0]}
				// {A4[统计信息][设备状态列表]}
				// {A4[6,5,8,5,6][1,0.0,50,0][2,1.2205148,20,2][3,0.0,20,0][4,0.20079458,30,1][5,1.9230052,30,3][6,0.0,40,0][7,1.0475571E7,60,4][8,0.6193658,10,1][9,0.0,70,0][10,1.2494619,10,2][11,1.5113857,90,3][12,0.59377354,50,1][13,1.4798579,20,2][14,1.4585748,60,2][15,0.6369066,10,1][16,1.0106955,30,2][17,6.4826324E7,70,4][18,1.47067,10,2][19,1.2162764,30,2][20,2.4697008E7,20,4][21,0.88702345,50,1][22,1.6779943,10,3][23,5.9165252E7,60,4][24,0.0,60,0][25,3.4982992E7,70,4][26,2.436245,10,3][27,0.0,30,0][28,1.3672788,20,2][29,1.8932029,50,3][30,3.9896016E7,20,4]}
				// [统计信息]： [离线数量,正常数量,中预警数量,高预警数量,报警数量]
				String realstr = msg.substring(3, msg.length() - 1);
				System.out.println(realstr);
				String[] strings = realstr.split("]");
				int index = 0;
				for (String alastr : strings) {
					// System.out.println(alastr);
					String[] values = alastr.substring(1).split(",");
					if (index == 0) {
						StatisticsInfo stinfo = new StatisticsInfo();
						stinfo.setOfflines(Integer.parseInt(values[0]));
						stinfo.setNormals(Integer.parseInt(values[0]));
						stinfo.setMiddleWarns(Integer.parseInt(values[0]));
						stinfo.setHighWarns(Integer.parseInt(values[0]));
						stinfo.setWarns(Integer.parseInt(values[0]));

						getDeviceHolder().addStatistics(stinfo);
					} else {
						String id = values[0];
						double rd = Double.parseDouble(values[1]);
						int qoe = Integer.parseInt(values[2]);
						int status = Integer.parseInt(values[3]);

						DeviceInfo device = getDeviceHolder().getDevice(id);
						if (device != null) {
							device.setLastData(rd, qoe, status);

							getDeviceHolder().addDataOfSeconds(id, rd);
						}
					}
					index++;
				}

			} else if (msg.startsWith("{A3") && msg.endsWith("]}")) {
				// 获取历史曲线图数据
				// {A3[27,0][0.0,0.0,8.5035192E7,2.3299556E7,0.0,0.54503167,0.0,0.0,0.0,4.9954436E7,0.12016626,0.0,1.0089909,3.2722032E7,0.0,7.9713488E7,0.5597268,2.4441814,0.0,1819862.9,1.0881251,8.485024E7,0.32643113,0.9132037,0.6795009,0.0,2.2104714,0.10343236,0.42792836,0.36044496,2.0912707,1.3404433,1.5721284,0.10233168,1.153811,2.1113348,1.3656018,0.95670676,1.3605665,1.5938135,9.3822768E7,1.1783184,0.451613,0.0,2.3395386,1.4145414,0.96947956,2.1469247,1.8908074E7,0.28108862,1.088972,2.2105227,1.1024166,7.7754688E7,0.9654322,0.0,0.0,2.1833153,0.28619832,1.8407042,1.8131119,2.4838626,0.0,1.3865321,0.14401546,0.0,0.0,2.4072995,0.0,0.6801754,1.5345651,1.6148633,1.7251067,5.7429296E7,0.0,0.0,2.1185257,0.0,0.12775731,0.24977453,0.13126871,1.2275896,1.0216329,1.7045429,3.7013724E7,3.0560276E7,0.4478406,7.2338736E7,1.0943264,0.1381427,1.401684,0.80798197,0.47897124,4.8466428E7,0.0,0.0,2.7367444E7,1.1176399,0.16940528,3.8967724E7,0.32055086,0.0,0.8392104,7.7182824E7,0.0,0.23492569,1.1789963,1.4733175,0.36769718,3.4137328E7,1.4497366,0.0,1.6575371,0.0,0.89449614,0.0,0.0,1205624.1,2.173877,0.6461698,0.0,1.7483156,1.1657456,0.24885799,5368385.5,3.610992E7,8.7546904E7,4.7732888E7,0.0,0.0,0.47265697,2.0341988,0.0,0.0,0.43021262,1.4509931,0.0,1.4613745,4247645.5,0.12419579,0.9816518,1.9314674E7,1.6581907,2.3784993,2.1033518,0.9176302,0.0,1.2238468,2.0939784,0.43027332,1.6655989,1.3667233,1.5746132,3.1650416E7,1.769122,9.3594688E7,2.4395466,0.95743316,0.54377556,0.84312946,1.2042881,1.7214054,0.70143956,0.0,0.3283122,1.0875373,1.842349,1.1157187,1.0538559,1.8064747,0.887309,2.4465883,0.0,0.6820861,0.0,1.4918052,0.5240258,0.0,2.1929295,0.0,1.5813434,7.9515768E7,1.9908984,5288204.0,2.1704862,2.004052,0.0,2.031927,0.94106984,0.96139425,0.0,2.4123418,1.3061852,1.4729515,0.0,1.7881671,0.81810504,3366361.0,2.1509051,0.13012853,0.0,1.3851851,1.3668284,1.5235558,3.4889232E7,3.8668792E7,1.6945773,1.4478081,1.1418393,0.32353884,1.0224242,1.3703545,0.5504617,0.43654233,0.9937989,2.1527784,0.9708256,7.082156E7,1.6912302,0.0,9.5197696E7,1.2323332,7.3952648E7,2.050971,0.5783973,3.0088962E7,0.55928254,1.7182083,1.3854426,2.2988894,1.5494788,1.6513697,0.3290077,1.7238277,1.5275754,0.44373754,1.3168601,0.0,0.0,0.0,0.0,4626890.0,2.1158757,1.0306548,1.3328245,0.0,0.0,1.7839516,0.0,0.0,3.6244904E7,0.69599587,2.8480426E7,0.0,0.0,1.3791385,8.9365896E7,8.4282712E7,2.4284613,1.4420615,4.8663864E7,0.0,0.86763775,1.4292182,1.0235934,0.0,1.0162354,1.3711325,2.253041,5.4988128E7,0.0,2.2939253,1.1330283,8.1987608E7,1.0362908,0.1852203,1.3429257,0.84408647,2.3368764,1.2036732,1.357509E7,0.0,1.0315472,2.9828132E7,0.0,1.5629312,1.3413782,3.207078E7,0.0,0.90347725,1.2449473,5.9851316E7,1.5140442,1.3261235,0.17986144,0.60349154,0.7852409,0.0,0.8135423,4.965426E7]}
				String hostorystr = msg.substring(3, msg.length() - 1);
				System.out.println(hostorystr);
				String[] strings = hostorystr.split("]");
				if (strings.length == 2) {
					String id = "";
					String[] heads = strings[0].substring(1).split(",");
					if (heads.length == 2) {
						id = heads[0];
					}
					if (StringUtils.isNotBlank(id)) {
						DeviceInfo device = getDeviceHolder().getDevice(id);
						if (device != null) {
							String[] values = strings[1].substring(1).split(",");
							for (String value : values) {
								double v = Double.parseDouble(value);
								getDeviceHolder().addDataOfSeconds(id, v);
							}
						}
					}
				}
			}
			logger.info("devices inited, device count " + getDeviceHolder().getDeviceCount());
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}

	public void queryDevices() throws URISyntaxException, UnsupportedEncodingException, InterruptedException {
		String msg = "{A0[admin,admin]}";
		// 获取建立的channel
		ChannelFuture cf = nettyClient.getChannelFuture();
		Channel channel = cf.channel();
		logger.info("获得的channel" + channel.toString());
		byte[] req = msg.getBytes();
		ByteBuf sendMsg = Unpooled.buffer(req.length);
		sendMsg.writeBytes(req);
		ChannelFuture channelFuture = cf.channel().writeAndFlush(sendMsg);
		logger.info("queryDevices: " + msg + " sending");
		channelFuture.await(2000);
		logger.info("queryDevices: " + msg + " finished");
	}

	public void queryAlarm() throws InterruptedException {
		String msg = "{A2}";
		// 获取建立的channel
		ChannelFuture cf = nettyClient.getChannelFuture();
		Channel channel = cf.channel();
		logger.info("BackMonitorService获得的channel" + channel.toString());
		byte[] req = msg.getBytes();
		ByteBuf sendMsg = Unpooled.buffer(req.length);
		sendMsg.writeBytes(req);
		ChannelFuture channelFuture = cf.channel().writeAndFlush(sendMsg);
		logger.info("queryAlarm: " + msg + " sending");
		channelFuture.await(2000);
		logger.info("queryAlarm: " + msg + " finished");
	}

	public void queryHistories(String devid) throws InterruptedException {
		// {A3[设备ID,参数]}
		// 0表示每秒钟数据
		// 1表示每分钟数据
		String msg = "{A3[" + devid + ",0]}";
		// 获取建立的channel
		ChannelFuture cf = nettyClient.getChannelFuture();
		Channel channel = cf.channel();
		logger.info("BackMonitorService获得的channel" + channel.toString());
		byte[] req = msg.getBytes();
		ByteBuf sendMsg = Unpooled.buffer(req.length);
		sendMsg.writeBytes(req);
		ChannelFuture channelFuture = cf.channel().writeAndFlush(sendMsg);

		logger.info("queryHistories: " + msg + " sending");
		channelFuture.await(2000);
		logger.info("queryHistories: " + msg + " finished");
	}

	public void queryRealtime(String... devids) throws InterruptedException {
		// {A3[设备ID,参数]}
		// 0表示每秒钟数据
		// 1表示每分钟数据
		String msg = "{A1[" + String.join(",", devids) + ",0]}";
		// 获取建立的channel
		ChannelFuture cf = nettyClient.getChannelFuture();
		Channel channel = cf.channel();
		logger.info("BackMonitorService获得的channel" + channel.toString());
		byte[] req = msg.getBytes();
		ByteBuf sendMsg = Unpooled.buffer(req.length);
		sendMsg.writeBytes(req);
		ChannelFuture channelFuture = cf.channel().writeAndFlush(sendMsg);

		logger.info("queryRealtime: " + msg + " sending");
		channelFuture.await(2000);
		logger.info("queryRealtime: " + msg + " finished");
	}

	public void queryRealtime() throws InterruptedException {
		// {A3[设备ID,参数]}
		// 0表示每秒钟数据
		// 1表示每分钟数据
		String msg = "{A4}";
		// 获取建立的channel
		ChannelFuture cf = nettyClient.getChannelFuture();
		Channel channel = cf.channel();
		logger.info("BackMonitorService获得的channel" + channel.toString());
		byte[] req = msg.getBytes();
		ByteBuf sendMsg = Unpooled.buffer(req.length);
		sendMsg.writeBytes(req);
		ChannelFuture channelFuture = cf.channel().writeAndFlush(sendMsg);

		logger.info("queryRealtime: " + msg + " sending");
		channelFuture.await(2000);
		logger.info("queryRealtime: " + msg + " finished");
	}

	@Override
	public void run() {
		running = true;
		while (running) {
			try {
				if (getDeviceHolder().getDeviceCount() == 0) {
					queryDevices();
				}
				if (getDeviceHolder().getDeviceCount() > 0) {
					// getDeviceHolder();
					DeviceInfo[] devices = getDeviceHolder().getDevices();
					for (DeviceInfo dev : devices) {
						int size = getDeviceHolder().getDataOfSecondsCount(dev.getId());
						if (size == 0) {
							queryHistories(dev.getId());
						} else {

						}
						Thread.sleep(1000);
						System.out.println("dev.id: " + dev.getId() + " DataOfSeconds: " + size);
					}
				}
				// 查询所有
				queryRealtime();

				Thread.sleep(3000);

				queryAlarm();
				System.out.println("querying device & alarm");
			} catch (Exception e) {

			}
			try {
				Thread.sleep(30000);
			} catch (InterruptedException e) {

			}
		}
	}

	@Override
	public void destroy() throws Exception {
		running = false;
	}
}
