package com.simmax.g206.service;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.simmax.g206.device.DeviceHolder;
import com.simmax.g206.device.DeviceInfo;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Service
public class BackMonitorJsonDaemon implements DisposableBean, Runnable, ClientListener {
	private static final Logger logger = LoggerFactory.getLogger(BackMonitorJsonDaemon.class);

	private Thread thread;
	private volatile boolean running;

	String logginguser = null;
	CountDownLatch lathc = new CountDownLatch(1);

	private static Map<String, String> customerids = new HashMap<String, String>();
	private Map<String, DeviceHolder> customers = new HashMap<String, DeviceHolder>();
	// @Autowired
	// private DeviceHolder deviceHolder;
	// 106.15.204.113:10300
	@Autowired
	NettyClient nettyClient;

	public BackMonitorJsonDaemon() {

	}

	@PostConstruct
	public void init() {
		nettyClient.addListener(this);
		logger.info("add listener to " + nettyClient);

		this.thread = new Thread(this);
	}

	public void start() {
		this.thread.start();
	}

	public DeviceHolder getDeviceHolder(String customerid) {
		// System.out.println(deviceHolder);
		if (!customers.containsKey(customerid)) {
			customers.put(customerid, new DeviceHolder());
		}
		return customers.get(customerid);
	}

	public void addCustomerId(String user, String customerid) {
		customerids.put(user, customerid);
	}

	public String getCustomerId(String user) {
		if (customerids.containsKey(user))
			return customerids.get(user);
		return null;
	}

	@Override
	public void channelRead(String msg) {
		System.out.println("====================");
		logger.info(msg);
		try {
			JSONObject response = JSONObject.fromObject(msg);
			String cmd = response.getString("cmd");
			if ("A0".equals(cmd)) {
				lathc.countDown();
				if (response.containsKey("customerId") && !StringUtils.isBlank(logginguser)) {
					addCustomerId(logginguser, response.getString("customerId"));
				} else {
					//登陆失败E1
				}
			} else if ("A1".equals(cmd)) {
				String customerid = response.getString("customerId");
				JSONArray data = response.getJSONArray("data");

				DeviceHolder holder = getDeviceHolder(customerid);
				for (int i = 0; i < data.size(); i++) {
					JSONObject device = data.getJSONObject(i);

					String devid = device.getString("ID");
					String value = device.getString("value");// name

					holder.addDevice(devid, value, 0);
				}

				logger.info("Got devices " + data.size());
			} else if ("A2".equals(cmd)) {
				String customerid = response.getString("customerId");
				JSONArray data = response.getJSONArray("data");

				for (int i = 0; i < data.size(); i++) {
					JSONObject alaram = data.getJSONObject(i);

					String devid = alaram.getString("ID");
					JSONArray values = alaram.getJSONArray("value");

					double rd = Double.parseDouble(values.getString(0));
					int qoe = Integer.parseInt(values.getString(1));
					int status = Integer.parseInt(values.getString(2));

					getDeviceHolder(customerid).addAlarm(devid, rd, qoe, status);
				}
			} else if ("A4".equals(cmd)) {
				String customerid = response.getString("customerId");
				JSONArray data = response.getJSONArray("data");

				for (int i = 0; i < data.size(); i++) {
					JSONObject alaram = data.getJSONObject(i);

					String devid = alaram.getString("ID");
					JSONArray values = alaram.getJSONArray("value");

					double rd = Double.parseDouble(values.getString(0));
					int qoe = Integer.parseInt(values.getString(1));
					int status = Integer.parseInt(values.getString(2));

					DeviceInfo device = getDeviceHolder(customerid).getDevice(devid);
					if (device != null) {
						device.setLastData(rd, qoe, status);

						// getDeviceHolder(customerid).getDataOfSeconds(id).add(rd);
					}
				}
			} else if ("A3".equals(cmd)) {
				String customerid = response.getString("customerId");
				JSONArray data = response.getJSONArray("data");

				String devid = response.getString("ID");
				String param = response.getString("param");

				for (int i = 0; i < data.size(); i++) {
					String value = data.getString(i);

					double v = Double.parseDouble(value);
					getDeviceHolder(customerid).addDataOfSeconds(devid, v);
				}
			}

			logger.info("inited, customerids/deviceholder count " + customerids.size() + "/" + customers.size());
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void sendChannel(JSONObject cmd) throws InterruptedException, UnsupportedEncodingException {
		ChannelFuture cf = nettyClient.getChannelFuture();
		Channel channel = cf.channel();
		logger.info("获得的channel" + channel.toString());
		byte[] req = cmd.toString().getBytes(nettyClient.config.getCharset());
		ByteBuf sendMsg = Unpooled.buffer(req.length);
		sendMsg.writeBytes(req);
		ChannelFuture channelFuture = cf.channel().writeAndFlush(sendMsg);
		logger.info("sending cmd: " + cmd);
		channelFuture.await(1000);
		logger.info("send finished");
	}

	public boolean login(String user, String pwd) throws InterruptedException, UnsupportedEncodingException {
		System.out.println(user + "is logining");
		JSONObject json = new JSONObject();
		json.put("cmd", "A0");
		json.put("userName", user);
		json.put("password", pwd);

		sendChannel(json);
		logginguser = user;
		lathc = new CountDownLatch(1);
		lathc.await(3, TimeUnit.SECONDS);

		String customerId = getCustomerId(user);
		return !StringUtils.isBlank(customerId);
	}

	public void queryDevices() throws URISyntaxException, UnsupportedEncodingException, InterruptedException {

	}

	public void queryAlarm(String user) throws InterruptedException, UnsupportedEncodingException {
		String customerId = getCustomerId(user);
		JSONObject json = new JSONObject();
		json.put("cmd", "A2");
		json.put("customerId", customerId);

		sendChannel(json);
	}

	public void queryHistories(String customerId, String devid)
			throws InterruptedException, UnsupportedEncodingException {
		JSONObject json = new JSONObject();
		json.put("cmd", "A3");
		json.put("customerId", customerId);
		json.put("ID", devid);
		json.put("param", "0");

		sendChannel(json);
	}

	public void queryRealtime(String... devids) throws InterruptedException {

	}

	public void queryRealtime() throws InterruptedException {

	}

	@Override
	public void run() {
		running = true;
		while (running) {
			try {
				// 查询所有
				// queryRealtime();

				Thread.sleep(1000);

				for (String customerid : customers.keySet()) {
					DeviceHolder holder = getDeviceHolder(customerid);
					DeviceInfo[] devices = holder.getDevices();
					for (DeviceInfo dev : devices) {
						int size = holder.getDataOfSecondsCount(dev.getId());
						if (size >= 0) {

						} else {

						}
						queryHistories(customerid, dev.getId());

						Thread.sleep(1000);
						System.out.println("dev.id: " + dev.getId() + " DataOfSeconds: " + size);
					}

					queryAlarm(customerid);
					Thread.sleep(1000);
				}
				System.out.println("querying device & alarm");
			} catch (Exception e) {

			}
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {

			}
		}
	}

	@Override
	public void destroy() throws Exception {
		running = false;
	}
}
