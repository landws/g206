package com.simmax.g206.service;

import java.net.InetSocketAddress;
import java.nio.charset.Charset;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.util.ReferenceCountUtil;

@Sharable
public class NettyClientHandler extends SimpleChannelInboundHandler<String> {

	private static final Logger logger = LoggerFactory.getLogger(NettyClientHandler.class);

	NettyClient client;

	public NettyClientHandler(NettyClient client) {
		this.client = client;
	}

	long startTime = -1;

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		InetSocketAddress insocket = (InetSocketAddress) ctx.channel().remoteAddress();
		String clientIp = insocket.getAddress().getHostAddress();
		logger.info("连接服务器[ip:" + clientIp + ":" + insocket.getPort() + "]成功");
		// ctx.writeAndFlush("active发送的消息");
	}

	StringBuilder buffer = new StringBuilder();

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		System.out.println("收到服务端消息: " + msg);
		ByteBuf in = (ByteBuf) msg;
		try {
			String charsetname = this.client.config.getCharset();
			Charset charset = Charset.forName(charsetname);
			String revstr = in.toString(charset);

			System.out.println(revstr.contains("\0") + "[" + revstr.contains("<EOF>") + "]" + revstr);
			buffer.append(revstr);
			if (revstr.indexOf("<EOF>") > -1 || revstr.endsWith("\0") || revstr.endsWith("\r\n")) {

				revstr = buffer.toString();
				buffer = new StringBuilder();

				if (!StringUtils.isBlank(revstr)) {
					this.client.notifyListeners(revstr);
				} else {
					logger.warn("received empty message");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ReferenceCountUtil.release(msg);
		}
	}

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, String msg) throws Exception {
		logger.info("收到服务端消息: " + msg);
	}

	@Override
	public void userEventTriggered(ChannelHandlerContext ctx, Object evt) {
		if (!(evt instanceof IdleStateEvent)) {
			return;
		}

		IdleStateEvent e = (IdleStateEvent) evt;
		logger.warn(e.state() + "," + e.isFirst());
		if (e.state() == IdleState.READER_IDLE) {
			// The connection was OK but there was no traffic for last period.
			logger.info("Disconnecting due to no inbound traffic");
			ctx.close();
		}
	}

	@Override
	public void channelInactive(final ChannelHandlerContext ctx) {
		logger.info("Disconnected from: " + ctx.channel().remoteAddress());
	}

	@Override
	public void channelUnregistered(final ChannelHandlerContext ctx) throws Exception {
		logger.info("Sleeping for: " + client.config.getReconnectDelay() + 's');

		ctx.channel().eventLoop().schedule(new Runnable() {
			@Override
			public void run() {
				logger.info("Reconnecting to: " + client.config.getHost() + ':' + client.config.getPort());
				client.connect();
			}
		}, client.config.getReconnectDelay(), TimeUnit.SECONDS);
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		cause.printStackTrace();
		ctx.close();
	}

	void println(String msg) {
		if (startTime < 0) {
			logger.error("[SERVER IS DOWN] %s%n", msg);
		} else {
			logger.error("[UPTIME: %5ds] %s%n", (System.currentTimeMillis() - startTime) / 1000, msg);
		}
	}
}
