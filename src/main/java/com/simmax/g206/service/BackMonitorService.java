package com.simmax.g206.service;

import java.util.Map;

import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.simmax.g206.device.DeviceHolder;
import com.simmax.g206.device.DeviceInfo;

@Service("remoteService")
public class BackMonitorService {
	private static final Logger logger = LoggerFactory.getLogger(BackMonitorService.class);

	// @Autowired
	// private BackMonitorDaemon daemon;

	@Autowired
	private BackMonitorJsonDaemon daemon;

	public BackMonitorService() {

	}

	@PreDestroy
	public void destory() {

	}

	public void start() {
		daemon.start();
	}

	public boolean login(String user, String pwd) {
		try {
			return daemon.login(user, pwd);
		} catch (Exception e) {
			logger.error("login fail: " + user, e);
		}
		return false;
	}

	private DeviceHolder getUserDeviceHolder(String user) {
		String customerid = daemon.getCustomerId(user);
		return daemon.getDeviceHolder(customerid);
	}

	public Object getDevice(String user, String id) {
		return getUserDeviceHolder(user).getDevice(id);
	}

	public Object[] getDevices(String user) {
		return getUserDeviceHolder(user).getDevices();
	}

	public Object getStatistics(String user) {
		return getUserDeviceHolder(user).getStatistics();
	}

	public Map getAlarms(String user) {
		return getUserDeviceHolder(user).getAlrams();
	}

	public Double[] getHistories(String user, String devid) {
		DeviceHolder holder = getUserDeviceHolder(user);
		DeviceInfo device = holder.getDevice(devid);
		if (device != null) {
			Double[] array = holder.getDataOfSeconds(devid);
			return array;
		}
		return new Double[] {};
	}

	public Object getRealtime(String user, String devid) {
		DeviceInfo device = getUserDeviceHolder(user).getDevice(devid);
		if (device != null) {
			return device.getLastData();
		}
		return null;
	}
}
