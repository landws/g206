package com.simmax.g206.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.timeout.IdleStateHandler;

@Component
public class NettyClient {
	private static final Logger logger = LoggerFactory.getLogger(NettyClient.class);

	private List<ClientListener> listeners = new ArrayList<ClientListener>();

	@Autowired
	public NettyConfig config;
	private EventLoopGroup group;
	private Bootstrap b;
	private ChannelFuture cf;

	@Autowired
	public NettyClient(NettyConfig config) {
		this.config = config;
		logger.info(this + " init nettyClient");
	}

	public synchronized void addListener(ClientListener listener) {
		if (!listeners.contains(listener)) {
			listeners.add(listener);
		}
	}

	public synchronized void removeListener(ClientListener listener) {
		if (listeners.contains(listener)) {
			listeners.remove(listener);
		}
	}

	protected synchronized void notifyListeners(String msg) {
		for (ClientListener listener : listeners) {
			listener.channelRead(msg);
		}
	}

	@PostConstruct
	public void init() throws InterruptedException {
		NettyClientHandler handler = new NettyClientHandler(this);
		logger.info(handler + " init handler");
		group = new NioEventLoopGroup();
		b = new Bootstrap();
		b.group(group).channel(NioSocketChannel.class).handler(new ChannelInitializer<SocketChannel>() {
			@Override
			protected void initChannel(SocketChannel sc) throws Exception {
				ChannelPipeline pipeline = sc.pipeline();

				logger.info("read timeout"+config.getReadTimeout());
				// pipeline.addLast(handler);
				pipeline.addLast(new IdleStateHandler(config.getReadTimeout(), 0, 0), handler);
			}
		});
		ChannelFuture cf = b.connect(config.getHost(), config.getPort()).sync();
	}

	public void connect() {
		try {
			this.cf = this.b.connect(config.getHost(), config.getPort()).sync(); // 发送json字符串
			logger.info("远程服务器已经连接, 可以进行数据交换..");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ChannelFuture getChannelFuture() {
		// 如果没有连接先链接
		if (this.cf == null) {
			this.connect();
		} // this.cf.channel().isActive() 这里得到的是链接状态
		if (!this.cf.channel().isActive()) {
			this.connect();
		}
		return this.cf;
	}

	// 释放资源
	public void close() {
		try {
			cf.channel().closeFuture().sync();
			group.shutdownGracefully();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
