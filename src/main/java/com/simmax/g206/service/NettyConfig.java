package com.simmax.g206.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class NettyConfig {
	@Value("${netty.host}")
	private String host;
	@Value("${netty.port}")
	private int port;

	@Value("${netty.readTimeout?:300}")
	private int readTimeout = 300;
	@Value("${netty.reconnectDelay?:60}")
	private int reconnectDelay = 60;

	private String charset="utf-8";
	
	public String getCharset() {
		return charset;
	}
	
	public String getHost() {
		return host;
	}

	public int getPort() {
		return port;
	}

	public int getReadTimeout() {
		return readTimeout;
	}

	public int getReconnectDelay() {
		return reconnectDelay;
	}

	@Bean
	public NettyClient nettyClient() {
		return new NettyClient(this);
	}
}
