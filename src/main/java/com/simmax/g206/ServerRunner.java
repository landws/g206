package com.simmax.g206;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.simmax.g206.service.BackMonitorService;

@Component
@Order(1)
public class ServerRunner implements CommandLineRunner {
	private final BackMonitorService monitor;
	private static final Logger logger = LoggerFactory.getLogger(ServerRunner.class);

	@Autowired
	public ServerRunner(BackMonitorService monitor) {
		this.monitor = monitor;
	}

	@Override
	public void run(String... args) throws Exception {
		logger.info("monitor is starting");
		this.monitor.start();
	}

}
