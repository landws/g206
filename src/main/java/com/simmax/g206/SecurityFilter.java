package com.simmax.g206;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.simmax.g206.controller.ServerController;

public class SecurityFilter extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		System.out.println("Begin to Filter session");
		HttpSession session = request.getSession();
		String user = (String) session.getAttribute(ServerController.SESSION_LOGINUSER);
		System.out.println("Current User = " + user);
		String currentpath = request.getRequestURL().toString();
		System.out.println("curent path:" + currentpath);
		if (currentpath.indexOf("/login") >= 0) {
			return true;
		}
		if (null == user || "".equals(user)) {
			response.setStatus(401);
			return true;
			/**
			 * handle session and security if you want.
			 */
			//request.getRequestDispatcher("/index.jsp").forward(request, response);
		}
		return super.preHandle(request, response, handler);
	}

}